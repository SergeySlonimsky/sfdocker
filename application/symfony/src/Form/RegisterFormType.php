<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => 25,
                    ]),
                    new NotBlank(),
                ],
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => 25,
                    ]),
                    new NotBlank(),
                    new Email(),
                ],
            ])
            ->add('password', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 6,
                        'max' => 25,
                    ]),
                    new NotBlank(),
                ],
            ]);

        parent::buildForm($builder, $options);
    }
}
