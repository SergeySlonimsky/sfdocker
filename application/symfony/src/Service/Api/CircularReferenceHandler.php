<?php

namespace App\Service\Api;

class CircularReferenceHandler
{
    public function __invoke($object)
    {
        return $object->getId();
    }
}
