<?php

namespace App\Service\Api;

class ApiResponseError
{
    public const ERROR_TYPE_VALIDATION = 'validation_error';
    public const ERROR_TYPE_INTERNAL = 'internal_error';
    public const ERROR_TYPE_BAD_REQUEST = 'request_error';

    /** @var string */
    private $type;

    /** @var string */
    private $target;

    /** @var string */
    private $message;

    /** @var array */
    private $inner;

    public function __construct(string $type, string $target, string $message, array $inner = [])
    {
        $this->type = $type;
        $this->target = $target;
        $this->message = $message;
        $this->inner = $inner;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget(string $target): void
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getInner(): array
    {
        return $this->inner;
    }

    public function addInner(ApiResponseError $inner)
    {
        $this->inner[] = $inner;
    }
}
