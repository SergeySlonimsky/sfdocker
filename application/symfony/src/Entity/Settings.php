<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user_settings")
 * @ORM\Entity()
 */
class Settings
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     */
    private $instLogin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     */
    private $instPass;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="settings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Settings
     */
    public function setId(int $id): Settings
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstLogin(): string
    {
        return $this->instLogin;
    }

    /**
     * @param string $instLogin
     * @return Settings
     */
    public function setInstLogin(string $instLogin): Settings
    {
        $this->instLogin = $instLogin;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstPass(): string
    {
        return $this->instPass;
    }

    /**
     * @param string $instPass
     * @return Settings
     */
    public function setInstPass(string $instPass): Settings
    {
        $this->instPass = $instPass;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Settings
     */
    public function setUser(User $user): Settings
    {
        $this->user = $user;
        return $this;
    }
}
