<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProfileController
 * @package App\Controller
 * @Route(path="/profile")
 */
class ProfileController extends ApiController
{
    /**
     * @return Response
     * @Route("/", name="profile", methods={"GET"})
     */
    public function profile(): Response
    {
        return $this->response($this->getUser());
    }
}
