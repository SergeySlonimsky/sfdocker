<?php

namespace App\Controller;

use App\Entity\Game;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GameController
 * @package App\Controller
 * @Route(path="/api/games")
 */
class GameController extends ApiController
{
    /**
     * @return Response
     * @Route("/", name="api_games_list", methods={"GET"})
     */
    public function games(Request $request): Response
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 10);
        $games = $this->getDoctrine()->getRepository(Game::class)->getGames($page, $limit);

        return $this->response($games);
    }

    /**
     * @return Response
     * @Route("/{id}", name="api_game", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function game(string $id): Response
    {
        return $this->response($this->getDoctrine()->getRepository(Game::class)->getGame($id));
    }
}
