<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /** @var array */
    private $headers = [
        'Content-Type' => 'application/json',
    ];

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    protected function response($data = null, int $status = Response::HTTP_OK, array $headers = []): Response
    {
        return new Response(
            $this->prepareData($data),
            $status,
            array_merge($this->headers, $headers)
        );
    }

    private function prepareData($data): string
    {
        return $this->serializer->serialize($data, 'json');
    }
}
