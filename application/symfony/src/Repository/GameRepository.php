<?php

namespace App\Repository;

use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function getGames(int $page, int $limit): array
    {
        $offset = ($page - 1) * $limit;

        return $this
            ->createQueryBuilder('g')
            ->select([
                'g.id',
                'g.name',
                'g.originalName',
                'g.slug',
                'g.year',
                'g.cover',
                'g.description'
            ])
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function getGame(int $id)
    {
        return $this
            ->createQueryBuilder('g')
            ->select([
                'g.id',
                'g.name',
                'g.originalName',
                'g.slug',
                'g.year',
                'g.cover',
                'g.description'
            ])
            ->where('g.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
