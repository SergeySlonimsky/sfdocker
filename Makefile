dev:
	docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.dev.yml up -d
prod:
	./docker/docker-compose up -d
restart:
	docker stop $(docker ps -q)
	docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.dev.yml up -d
